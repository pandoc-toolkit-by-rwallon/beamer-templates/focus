---
# Base Preamble Settings
handout:
language:
package:
    -
header-includes:
    -
include-before:
    -

# Color Definitions
color:
    - name:
      type:
      value:

# Color Settings
main-color:
background-color:
alert-color:
box-color:

# Font Settings
nofirafonts:
sans-font:
mono-font:

# Frame Numbering
numbering:

# Pandoc Integration
bold:
quote:
toc:
links-as-notes:

# Personal Settings
title:
subtitle:
author:
    -
institute:
    -

# Event Information
event:
date:

# Logos
logo:
    -
logo-height:

# Final Highlights
highlights-title:
highlights-width:
highlights-rows:
    - frames:
        -

# Bibliography
biblio-style:
natbiboptions:
    -
bibliography:
    -
---
