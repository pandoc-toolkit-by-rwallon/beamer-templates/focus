% ----------------------------------------------------------------------------
% Pandoc Template for the Focus Beamer Theme.
% Copyright (c) 2019-2021 - Romain Wallon.
%
% The original Focus Theme is designed by Pasquale Africa.
%
% This template is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the Free
% Software Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This template is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
% FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along with
% this program.
% If not, see <http://www.gnu.org/licenses/>.
% ----------------------------------------------------------------------------

% -------------------------- PACKAGE CONFIGURATION ---------------------------

\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}

% -------------------------- DOCUMENT CONFIGURATION --------------------------

\documentclass[10pt, $if(handout)$handout,$endif$ usenames, dvipsnames]{beamer}
\usetheme[$if(numbering)$numbering=$numbering$,$endif$ $if(nofirafonts)$nofirafonts$endif$]{focus}

% ----------------------------- PACKAGE LOADING ------------------------------

% Language.
\usepackage[$if(language)$$language$$else$english$endif$]{babel}

% Colors.
\usepackage{xcolor}

% Tables.
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{tabularx}

% Figure customization.
\usepackage{graphicx, grffile}
\usepackage{float}

% Syntax highlighting.
\usepackage{listings}

% User-specific packages.
$for(package)$
\usepackage{$package$}
$endfor$

% --------------------------- COLOR CONFIGURATION ----------------------------

$for(color)$
\definecolor{$color.name$}{$color.type$}{$color.value$}
$endfor$


$if(main-color)$
\colorlet{main}{$main-color$}
$endif$

$if(background-color)$
\colorlet{background}{$background-color$}
$endif$

$if(alert-color)$
\setbeamercolor{alerted text}{fg=$alert-color$}
$endif$

$if(box-color)$
\beamerboxesdeclarecolorscheme{punchline}{main}{$box-color$}
$else$
\definecolor{fLightGray}{HTML}{D8D8D8}
\beamerboxesdeclarecolorscheme{punchline}{main}{fLightGray}
$endif$

% -------------------------------- FONT STYLE --------------------------------

$if(sans-font)$
\setsansfont{$sans-font$}
$endif$

$if(mono-font)$
\setmonofont{$mono-font$}
$endif$

% ------------------------ BIBLIOGRAPHY CUSTOMIZATION ------------------------

$if(bibliography)$
\usepackage[$for(natbiboptions)$$natbiboptions$$sep$,$endfor$]{natbib}
\bibliographystyle{$if(biblio-style)$$biblio-style$$else$plainnat$endif$}
$endif$

% ---------------------------- PANDOC INTEGRATION ----------------------------

$if(bold)$
% Bold font is left as is.
$else$
% Bold font is replaced by Alert font (this is the default).
\renewcommand{\textbf}[1]{\alert{#1}}
$endif$

$if(quote)$
% Quote environment is left as is.
$else$
% Quote environment is redefined to be used for punchlines.
\renewenvironment{quote}{
    \bigskip
    \begin{beamerboxesrounded}[scheme=punchline, shadow=true]{}
        \centering
        \it
}{
    \end{beamerboxesrounded}
}
$endif$

% Various style settings.
\newcolumntype{Y}{>{\centering\arraybackslash}X}
\providecommand{\tightlist}{\setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

$if(links-as-notes)$
% Links are considered as footnotes.
\DeclareRobustCommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$

% --------------------------- SYNTAX HIGHLIGHTING ----------------------------

\newcommand{\passthrough}[1]{#1}

\lstset{defaultdialect=[5.3]Lua}
\lstset{defaultdialect=[x86masm]Assembler}

$highlighting-macros$

% ---------------------------- FIGURE POSITIONING ----------------------------

\makeatletter

% Maximum width for the figures.
\def \maxwidth {
    \ifdim \Gin@nat@width > \linewidth
        0.9\linewidth
    \else
        \Gin@nat@width
    \fi
}

% Maximum height for the figures.
\def \maxheight {
    \ifdim \Gin@nat@height > \textheight
        0.7\textheight
    \else
        \Gin@nat@height
    \fi
}

\makeatother

% Setting up figures to fit the slides.
\setkeys{Gin}{width=\maxwidth, height=\maxheight, keepaspectratio}
\let\originalfigure\figure
\let\originalendfigure\endfigure
\renewenvironment{figure}{\originalfigure}{\originalendfigure}

% ----------------------------- CUSTOM PREAMBLE ------------------------------

$for(header-includes)$
$header-includes$
$endfor$

$for(include-before)$
$include-before$
$endfor$

% -------------------------- PRESENTATION METADATA ---------------------------

\title{$title$}

$if(subtitle)$
\subtitle{$subtitle$}
$endif$

\date{$if(event)$$event$ -- $endif$$if(date)$$date$$else$\today$endif$}

\author{$for(author)${$author$}$sep$,$endfor$}
\institute{$for(institute)$$institute$$sep$\\$endfor$}


$if(logo)$
\titlegraphic{
    \begin{table}
        \hfill
        \begin{tabularx}{0.25\textwidth}{c}
$for(logo)$
            \includegraphics[height=$if(logo-height)$$logo-height$$else$15mm$endif$]{$logo$}\\$sep$\\
$endfor$
        \end{tabularx}
        \hspace{1cm}
    \end{table}
}
$endif$

% ---------------------------------- DOCUMENT --------------------------------

\begin{document}

% The opening slide.
\begin{frame}
    \maketitle
\end{frame}

$if(toc)$
% The slide showing the table of contents.
\begin{frame}{\contentsname}
    \setbeamertemplate{section in toc}[sections numbered]
    \tableofcontents[hideallsubsections]
\end{frame}
$endif$

% The actual content of the presentation.
$body$

$if(highlights)$
% The highlights of the talk, as a conclusion.
\begin{frame}$if(highlights-title)$$else$[plain]$endif$
    $if(highlights-title)$\frametitle{$highlights-title$}$endif$
$if(no-highlights)$
$else$
    \setlength{\tabcolsep}{10pt}
    \begin{tabularx}{\textwidth}{YYYY}
$for(highlights-rows)$
$for(highlights-rows.frames)$
        \includegraphics[%
            width=$if(highlights-width)$$highlights-width$$else$3cm$endif$,%
            page=$highlights-rows.frames$%
        ]{%
            $highlights$%
        }$sep$&
$endfor$$sep$\\
$endfor$
    \end{tabularx}
$endif$
\end{frame}
$endif$

% The closing slide.
\begin{frame}
    \maketitle
\end{frame}

$if(backup)$
% The backup slides.
\appendix
\input{$backup$}

% The "real" closing slide.
\begin{frame}
    \maketitle
\end{frame}
$endif$

$if(bibliography)$
% Configuring the style for the bibliography.
\metroset{sectionpage=none}
\metroset{subsectionpage=none}
\setbeamertemplate{frametitle continuation}{}

% Showing the bibliography.
\begin{frame}[allowframebreaks,noframenumbering]{\bibname}
    \bibliography{$for(bibliography)$$bibliography$$sep$,$endfor$}
\end{frame}
$endif$

\end{document}
