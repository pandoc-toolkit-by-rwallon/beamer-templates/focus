---
# Personal Settings
title: Pandoc Template for Focus
subtitle: A Minimalist Beamer Theme
author: Romain Wallon
institute: Pandoc Toolkit

# Event Information
event: Release 0.1.0
date: October 2019

# Logos
logo:
    - logos/markdown.png
    - logos/latex.png
logo-height: 1.4cm
---

# Introduction

## From Focus to Pandoc

### Focus

**Focus** is a presentation theme for LaTeX Beamer that aims at a clean and
minimalist design, so to minimize distractions and put the focus directly on
the content.

*Focus* was initially created and designed by Pasquale Africa.

Note that you have to have Mozilla's *Fira Sans* font and `XeLaTeX` installed
to enjoy this wonderful typography.

### Pandoc

**Pandoc** is a Haskell library for converting from one markup format to
another, and a command-line tool that uses this library.

### Why a Pandoc Template for Focus?

To use *Focus* with Pandoc-based presentations, you may run the following
command:

```bash
$ pandoc --pdf-engine=xelatex \
         -t beamer -V theme:focus \
         -o output.pdf input.md
```

However, this often requires to write `LaTeX` code if you want to customize
your presentation.

We propose instead to design a highly-customizable Pandoc template for
*Focus*.

# Writing your Presentation

## Elements

### Typography

Because Markdown is limited in terms of typography, you cannot combine bold and
alert fonts:

```markdown
The theme provides sensible defaults to
*emphasize* text, and to show **strong**
results.
```

becomes

The theme provides sensible defaults to *emphasize* text, and to show
**strong** results.

### Lists

There is a support for items:

+ Milk
+ Eggs
+ Potatoes

Enumerations are also supported:

1. First,
2. Second and
3. Last.

### Animations

Unfortunately, Markdown does not support Beamer animations...

\pause

But you can still write \LaTeX!

\pause

Simply put a `\pause`, and it will work perfectly!

### Figures

![This presentation is written in Mardown.](logos/markdown.png)

### Tables

Tables are supported through Pandoc's Markdown.

The largest cities in the world (source: Wikipedia) are:

| City         | Population  |
|--------------|-------------|
| Mexico City  | 20,116,842  |
| Shanghai     | 19,210,000  |
| Peking       | 15,796,450  |
| Istanbul     | 14,160,467  |

### Punchlines

*Punchlines* are implemented through the `quote` environment, unless you
specified that you did not want them:

```markdown
> Isn't it great?
```

They are used for *plot-twists* in your presentations, or for *take-away
messages*.

\pause

> Isn't it great?

# Conclusion

### Conclusion

Get the source of the theme on
[GitHub](https://github.com/elauksap/focus-beamertheme).

Get the source of the template and all the examples on
[GitLab](https://gitlab.com/pandoc-toolkit-by-rwallon/beamer-templates/focus).

> Both the theme and the template are licensed under the GNU General Public
> License, version 3.
