---
# Color Definitions
color:
    - name: mymain
      type: RGB
      value: 92,138,168
    - name: mybackground
      type: RGB
      value: 240,247,255
    - name: myalert
      type: RGB
      value: 0,191,255
    - name: mybox
      type: RGB
      value: 173,216,230

# Color Settings
main-color: mymain
background-color: mybackground
alert-color: myalert
box-color: mybox

# Font Settings
nofirafonts: true

# Frame Numbering
numbering: fullbar

# Pandoc Integration
toc: true
---
