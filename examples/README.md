# Use Cases of the *Focus* Template

This directory contains examples illustrating different use cases of the Pandoc
template for the *Focus* Beamer theme.

All the examples are based on the same Markdown source, available
[here](./example.md).
The different outputs are only obtained by using different metadata files.

Observe that, in these examples, slide titles are level-3 titles in the
Markdown source.
Hence, `SLIDE_LEVEL` has to be set to `3`.

For each of the examples, you may either read its metadata file (to see how we
configured it) or download the final PDF.

## Default Configuration

In this example, there are no additional settings.
The presentation is produced based on the default configuration.

*Metadata file available [here](default.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/default.pdf?job=make-examples).*

## Original *Focus*

This example is configured so that the presentation uses the original default
configuration of the *Focus* theme.

*Metadata file available [here](original.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/original.pdf?job=make-examples).*

## Custom Style

This example does not use the default *Fira* font, and a set of user-defined
colors replaces the original colors.

The frame numbering does not use a progress bar.

A table of content is added at the beginning of the presentation.

*Metadata file available [here](custom-style.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/custom-style.pdf?job=make-examples).*
